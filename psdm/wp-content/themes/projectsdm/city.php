<?php
/*
 Controller name: Cit
 Controller description: JSON API CITY Controller
 */
 // require_once MY_THEME_FOLDER . '/header.php';
class JSON_API_City_Controller {

  // public $defaultLogo = 'https://s3-ap-south-1.amazonaws.com/runcation-app/admin/wp-content/uploads/2019/06/19141353/mp-tourism-640-1.jpg';
  // public $defaultThumnail = 'https://s3-ap-south-1.amazonaws.com/runcation-app/admin/wp-content/uploads/2019/06/19141220/mp-tourism-120.jpg';
    
  public function get_category_details() {
    global $json_api;
    
    $images = get_option('taxonomy_image_plugin');
    for($i = 2 ; $i <=count($images)+1; $i++){
     $img_url[$i]= wp_get_attachment_url( $images[$i] );
    }
    $categories = $json_api->introspector->get_categories();

    $destinations =[]; $i = 0;
    foreach ($categories as $cat) {
      $destinations[$i]['id']= $cat->id;  
      $destinations[$i]['slug']= $cat->slug; 
      $destinations[$i]['title']= $cat->title; 
      $destinations[$i]['description']= $cat->description;
      $destinations[$i]['location']= implode( get_term_meta( $cat->id, 'location'));
      $destinations[$i]['booking']= implode(get_term_meta( $cat->id, 'booking'));
      $destinations[$i]['ad_link'] = implode(get_term_meta( $cat->id, 'ad_link'));
      $somevariable = $images[$cat->id];
      if(empty($somevariable) OR $somevariable == ''){
        $destinations[$i]['image']= $this->defaultLogo;
      }
      else{
        $destinations[$i]['image']= wp_get_attachment_url( $images[$cat->id] );
      }
      $i++;
    } 

    if(empty($destinations)) {
     return ['status' => 0, 'message' => 'No Destination Available'];
    }
    else{
      return ['results' => $destinations, 'status' => 1  ];
    }
  }
 
  public function get_category_posts() {
    global $json_api;
    $category = $json_api->introspector->get_current_category();
    if (!$category) {
      $json_api->error("Not found.");
    }
    $posts = $json_api->introspector->get_posts(array(
      'cat' => $category->id,
      'post_type' => array('run','read','play','view'),
      'meta_key'      => 'promote',
      'orderby'     => 'meta_value',
      'order' => 'DESC',
      'posts_per_page' => '-1'
    ));
    // print_r($posts);
    $newpostarray = []; $i=0;
    foreach ($posts as $key => $post) {
      $postID = $post->id;
      $newpostarray[$i]['id']= $postID;
      $newpostarray[$i]['title']= html_entity_decode(get_the_title($postID));
      $newpostarray[$i]['type']= get_post_type($postID);
      $newpostarray[$i]['slug']= $post->slug;
      $newpostarray[$i]['url']= get_post_permalink($postID);
      $newpostarray[$i]['status']= $post->status;
      $newpostarray[$i]['content']= $post->content;
      $newpostarray[$i]['date']= $post->date;
      $newpostarray[$i]['modified']= $post->modified;
      $newpostarray[$i]['tags']= $post->tags;
      $newpostarray[$i]['comments']= $post->comments;
      $newpostarray[$i]['comment_status']= $post->comment_status;
      
      $postImage = get_the_post_thumbnail_url($post->id);
      if(empty($postImage) OR $postImage == ''){
        $newpostarray[$i]['featured_image'] = $this->defaultLogo;
      }
      else{
        $newpostarray[$i]['featured_image'] = get_the_post_thumbnail_url($post->id, 'full_image');
      }
      $postThumbnail = get_the_post_thumbnail_url($post->id);
      if(empty($postThumbnail) OR $postThumbnail == ''){
        $newpostarray[$i]['thumbnail'] = $this->defaultThumnail;
      }
      else{
        $newpostarray[$i]['thumbnail'] = get_the_post_thumbnail_url($post->id, 'thumbnail_image');
      }
      //this is run post
      $somevariable = implode('', $post->custom_fields->description);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['description']= implode('', $post->custom_fields->description);
      }
      $somevariable = implode('', $post->custom_fields->distance);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['distance']= implode('', $post->custom_fields->distance);
      }
      $somevariable = implode('', $post->custom_fields->route);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['route']= implode(', ', get_field('route',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->video_play_back_time);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['video_play_back_time']= implode('', $post->custom_fields->video_play_back_time);
      }
      $somevariable = implode('', $post->custom_fields->features_of_route);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['features_of_route']= implode(', ', get_field('features_of_route',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->gpx_file);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['gpx_file']= implode('', $post->custom_fields->gpx_file);
      }
      //this is read post
      $somevariable = implode('', $post->custom_fields->description_read);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['description_read']= implode('', $post->custom_fields->description_read);
      }
      $somevariable = implode('', $post->custom_fields->estimated_reading_time);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['estimated_reading_time']= implode('', $post->custom_fields->estimated_reading_time);
      }
      $somevariable = implode('', $post->custom_fields->features_key_points);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['features_key_points']= implode(', ', get_field('features_key_points',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->read_site_url);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['read_site_url']= implode('', $post->custom_fields->read_site_url);
      }
      // //this is view post
      $somevariable = implode('', $post->custom_fields->description_view);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['description_view']= implode('', $post->custom_fields->description_view);
      }
      $somevariable = implode('', $post->custom_fields->features_seen_view);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['features_seen_view']=  implode(', ', get_field('features_seen_view',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->view_image_url);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['view_image_url']= implode('', $post->custom_fields->view_image_url);
      }
      $somevariable = implode('', $post->custom_fields->view_type);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['view_type']= implode('', $post->custom_fields->view_type);
      }
      $somevariable = implode('', $post->custom_fields->route_view);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['route_view']=  implode(', ', get_field('route_view',$post->id));
      }
      //this is play post
      $somevariable = implode('', $post->custom_fields->description_of_game);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['description_of_game']= implode('', $post->custom_fields->description_of_game);
      }
      $somevariable = implode('', $post->custom_fields->type_of_game);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['type_of_game']= implode(', ', get_field('type_of_game',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->estimated_time);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['estimated_time']= implode('', $post->custom_fields->estimated_time);
      }
      $somevariable = implode('', $post->custom_fields->skill_level);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['skill_level']= implode('', $post->custom_fields->skill_level);
      }
      /*Gamify fields only for play*/
      $somevariable = implode('', $post->custom_fields->base_url);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['base_url']= implode('', $post->custom_fields->base_url);
      }
      $somevariable = implode('', $post->custom_fields->partner_id);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['partner_id']= implode('', $post->custom_fields->partner_id);
      }
      $somevariable = implode('', $post->custom_fields->partner_token);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['partner_token']= implode('', $post->custom_fields->partner_token);
      }
      $somevariable = implode('', $post->custom_fields->user_id);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['user_id']= implode('', $post->custom_fields->user_id);
      }
      $somevariable = implode('', $post->custom_fields->city_id);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['city_id']= implode('', $post->custom_fields->city_id);
      }
      $somevariable = implode('', $post->custom_fields->treasure_hunt_id);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['treasure_hunt_id']= implode('', $post->custom_fields->treasure_hunt_id);
      }
      $somevariable = implode('', $post->custom_fields->game_site_url);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['game_site_url']= implode('', $post->custom_fields->game_site_url);
      }
      $somevariable = implode('', $post->custom_fields->promote);
      if(!empty($somevariable) OR $somevariable != ''){ 
        if($post->custom_fields->promote !='')
        $newpostarray[$i]['promote']= true;
      } else {
          $newpostarray[$i]['promote']= false;
      }
      $somevariable = implode('', $post->custom_fields->post_ad_field);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['post_ad_field']= implode('', $post->custom_fields->post_ad_field);
      } else {
        $newpostarray[$i]['post_ad_field']= "";
      }
      $somevariable = implode('', $post->custom_fields->organiser);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['organiser']= implode('', $post->custom_fields->organiser);
      }
      $organiser_image = get_field('organiser_image',$post->id); 
      if(!empty($organiser_image) OR $organiser_image != ''){ 
        $newpostarray[$i]['organiser_image']= $organiser_image;
      }
      $organised_location = get_field('organised_location',$post->id); 
      if(!empty($organised_location) OR $organised_location != ''){ 
        $newpostarray[$i]['organised_location']= $organised_location;
      }
      $destinationName = get_the_category($postID);
      $newpostarray[$i]['destination_title']= $destinationName[0]->name;
      $i++;
    }
    if (empty($newpostarray)) {
     return ['status' => 0, 'message' => 'No Destination Available'];
    }
    else{
      return ['results' => $newpostarray, 'status' => 1 ];
    }
  }

  public function get_post() {
    global $json_api, $post;
    $post = $json_api->introspector->get_current_post();
    if ($post) {
      $posts = array(
        'post' => new JSON_API_Post($post)
      );
      $newpostarray = []; $i=0;
      foreach ($posts as $key => $post) {
        $postID = $post->id;
        $newpostarray[$i]['id']= $post->id;
        $newpostarray[$i]['title']= html_entity_decode(get_the_title($postID));
        $newpostarray[$i]['type']= $post->type;
        $newpostarray[$i]['slug']= $post->slug;
        $newpostarray[$i]['url']= $post->url;
        $newpostarray[$i]['status']= $post->status;
        $newpostarray[$i]['content']= $post->content;
        $newpostarray[$i]['date']= $post->date;
        $newpostarray[$i]['modified']= $post->modified;
        $newpostarray[$i]['tags']= $post->tags;
        $newpostarray[$i]['comments']= $post->comments;
        $newpostarray[$i]['comment_status']= $post->comment_status;
        $newpostarray[$i]['comments']= $post->comments;
        $postImage = get_the_post_thumbnail_url($post->id);
        if(empty($postImage) OR $postImage == ''){
          $newpostarray[$i]['image'] = $this->defaultLogo;
        }
        else{
          $newpostarray[$i]['image'] = get_the_post_thumbnail_url($post->id, 'full_image');
        }
        //this is run post
        $somevariable = implode('', $post->custom_fields->description);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['description']= implode('', $post->custom_fields->description);
        }
        $somevariable = implode('', $post->custom_fields->distance);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['distance']= implode('', $post->custom_fields->distance);
        }
        $somevariable = implode('', $post->custom_fields->route);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['route']= implode(', ', get_field('route',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->video_play_back_time);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['video_play_back_time']= implode('', $post->custom_fields->video_play_back_time);
        }
        $somevariable = implode('', $post->custom_fields->features_of_route);
        if(!empty($somevariable) OR $somevariable != ''){
          $newpostarray[$i]['features_of_route']= implode(', ', get_field('features_of_route',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->link_to_bookingcom);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['link_to_bookingcom']= implode('', $post->custom_fields->link_to_bookingcom);
        }
        $somevariable = implode('', $post->custom_fields->gpx_file);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['gpx_file']= implode('', $post->custom_fields->gpx_file);
        }
        //this is read post
        $somevariable = implode('', $post->custom_fields->description_read);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['description_read']= implode('', $post->custom_fields->description_read);
        }
        $somevariable = implode('', $post->custom_fields->estimated_reading_time);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['estimated_reading_time']= implode('', $post->custom_fields->estimated_reading_time);
        }
        $somevariable = implode('', $post->custom_fields->features_key_points);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['features_key_points']= implode(', ', get_field('features_key_points',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->read_site_url);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['read_site_url']= implode('', $post->custom_fields->read_site_url);
        }
        // //this is view post
        $somevariable = implode('', $post->custom_fields->description_view);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['description_view']= implode('', $post->custom_fields->description_view);
        }
        $somevariable = implode('', $post->custom_fields->features_seen_view);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['features_seen_view']= implode(', ', get_field('features_seen_view',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->view_image_url);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['view_image_url']= implode('', $post->custom_fields->view_image_url);
        }
        $somevariable = implode('', $post->custom_fields->view_type);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['view_type']= implode('', $post->custom_fields->view_type);
        }
        
        $somevariable = implode('', $post->custom_fields->route_view);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['route_view']= implode(', ', get_field('route_view',$post->id));
        }
        //this is play post
        $somevariable = implode('', $post->custom_fields->description_of_game);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['description_of_game']= implode('', $post->custom_fields->description_of_game);
        }
        $somevariable = implode('', $post->custom_fields->type_of_game);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['type_of_game']= implode(', ', get_field('type_of_game',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->estimated_time);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['estimated_time']= implode('', $post->custom_fields->estimated_time);
        }
        $somevariable = implode('', $post->custom_fields->skill_level);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['skill_level']= implode('', $post->custom_fields->skill_level);
        }
        /*Gamify fields only for play*/
        $somevariable = implode('', $post->custom_fields->base_url);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['base_url']= implode('', $post->custom_fields->base_url);
        }
        $somevariable = implode('', $post->custom_fields->partner_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['partner_id']= implode('', $post->custom_fields->partner_id);
        }
        $somevariable = implode('', $post->custom_fields->partner_token);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['partner_token']= implode('', $post->custom_fields->partner_token);
        }
        $somevariable = implode('', $post->custom_fields->user_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['user_id']= implode('', $post->custom_fields->user_id);
        }
        $somevariable = implode('', $post->custom_fields->city_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['city_id']= implode('', $post->custom_fields->city_id);
        }
        $somevariable = implode('', $post->custom_fields->treasure_hunt_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['treasure_hunt_id']= implode('', $post->custom_fields->treasure_hunt_id);
        }
        $somevariable = implode('', $post->custom_fields->game_site_url);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['game_site_url']= implode('', $post->custom_fields->game_site_url);
        }
        $somevariable = implode('', $post->custom_fields->promote);
        if(!empty($somevariable) OR $somevariable != ''){ 
          if($post->custom_fields->promote !='')
          $newpostarray[$i]['promote']= true;
        } else {
            $newpostarray[$i]['promote']= false;
        }
        $somevariable = implode('', $post->custom_fields->post_ad_field);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['post_ad_field']= implode('', $post->custom_fields->post_ad_field);
        } else {
          $newpostarray[$i]['post_ad_field']= "";
        }
        $somevariable = implode('', $post->custom_fields->organiser);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['organiser']= implode('', $post->custom_fields->organiser);
        }
        $organiser_image = get_field('organiser_image',$post->id); 
        if(!empty($organiser_image) OR $organiser_image != ''){ 
          $newpostarray[$i]['organiser_image']= $organiser_image;
        }
        $organised_location = get_field('organised_location',$post->id); 
        if(!empty($organised_location) OR $organised_location != ''){ 
          $newpostarray[$i]['organised_location']= $organised_location;
        }
    }
      if (empty($newpostarray)) {
        return ['status' => 0, 'message' => 'No Destination Available'];
      }
      else{
        return ['results' => $newpostarray, 'status' => 1 ];
      }

    } 
    else {
      $json_api->error("Not found.");
    }
  }

  public function get_page() {
    global $json_api;
    extract($json_api->query->get(array('id', 'slug', 'page_id', 'page_slug', 'children')));
    if ($id || $page_id) {
      if (!$id) {
        $id = $page_id;
      }
      $posts = $json_api->introspector->get_posts(array(
        'page_id' => $id
      ));
    } else if ($slug || $page_slug) {
      if (!$slug) {
        $slug = $page_slug;
      }
      $posts = $json_api->introspector->get_posts(array(
        'pagename' => $slug
      ));
    }
     else {
      $json_api->error("Include 'id' or 'slug' var in your request.");
    }
    
    $theContent = get_the_content();
    $stripTags = wp_strip_all_tags($theContent);
    $page = []; $i=0;
    foreach ($posts as $post) {
      $page[$i]['id'] = $post->id;
      $page[$i]['type'] = $post->type;
      $page[$i]['slug'] = $post->slug;
      $page[$i]['status'] = $post->status;
      $page[$i]['title'] = $post->title;
      $page[$i]['content'] = $stripTags;
    }
    if (empty($page)) {
      return ['status' => 0, 'message' => 'No Page Available'];
    }
    else{
      return ['results' => $page, 'status' => 1 ];
    }
  }

  public function get_search_results(){
    global $wpdb;
    $searchVariable = $_REQUEST['search'];
    if (empty($searchVariable)) {
      return ['status' => 0, 'message' => 'Enter atleast 3 letters'];
    }
    $searchSql = "SELECT term_id FROM wp_terms WHERE name LIKE '%$searchVariable%'";
    // print_r($searchSql);
    $searchResult = $wpdb->get_results($searchSql);

    if (empty($searchResult) || $searchResult == '') {
      $myposts = $wpdb->get_results( "SELECT ID FROM wp_posts WHERE post_title LIKE '%$searchVariable%'");
      if (empty($myposts) || $myposts == '') {
        return ['status' => 0, 'message' => 'Results not found'];
      }
      $searchPostQuery= []; $i=0;
      foreach ($myposts as $existPost) {
        $searchPostQuery[$i] = $existPost->ID;
        $i++;
      }
      // print_r($searchPostQuery);
      $postsSearch = get_posts(array(
        'post__in' => $searchPostQuery,
        'post_type' => array('post'),
        'meta_key'      => 'promote',
        'orderby'     => 'meta_value',
        'order' => 'DESC',
        'posts_per_page' => '-1'
      ));
      // print_r($postsSearch);
      $finalPostSearch = []; $j=0;
      foreach ($postsSearch as $key => $post) {
        $POSTID = $post->ID;
        $finalPostSearch[$j]['id'] = $post->ID;
        $finalPostSearch[$j]['slug'] = $post->post_name;
        $finalPostSearch[$j]['title'] = $post->post_title;
        $finalPostSearch[$j]['type'] = $post->post_type;
        $finalPostSearch[$j]['url'] = get_permalink($POSTID);
        $finalPostSearch[$j]['status'] = $post->post_status;
        $finalPostSearch[$j]['content'] = $post->post_content;
        $finalPostSearch[$j]['tags'] = $post->tags;
        $finalPostSearch[$j]['comments'] = $post->comments;
        $finalPostSearch[$j]['comment_status'] = $post->comment_status;
        //condition for thumbnail
        $postImage = get_the_post_thumbnail_url($post->ID);
        if(empty($postImage) OR $postImage == ''){
          $finalPostSearch[$j]['featured_image'] = $this->defaultLogo;
        }
        else{
          $finalPostSearch[$j]['featured_image'] = get_the_post_thumbnail_url($post->ID, 'full_image');
        }
        $postThumbnail = get_post_thumbnail_id($post->ID);
        if(empty($postThumbnail) OR $postThumbnail == ''){
          $finalPostSearch[$j]['thumbnail'] = $this->defaultLogo;
        }
        else{
          $finalPostSearch[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
        }
        //run fields
        $somevariable = get_field('description',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['description']= get_field('description',$post->ID);
        }
        $somevariable = get_field('distance',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['distance']= get_field('distance',$post->ID);
        }
        $somevariable = get_field('route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['route']= implode(', ', get_field('route',$post->ID));
        }
        $somevariable = get_field('video_play_back_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['video_play_back_time']= get_field('video_play_back_time',$post->ID);
        }
        $somevariable = get_field('features_of_route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['features_of_route'] = implode(', ', get_field('features_of_route',$post->ID));
        }
        $somevariable = get_field('gpx_file',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['gpx_file']= get_field('gpx_file',$post->ID);
        }
        //read fields
        $somevariable = get_field('description_read',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['description_read']= get_field('description_read',$post->ID);
        }
        $somevariable = get_field('estimated_reading_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['estimated_reading_time']= get_field('estimated_reading_time',$post->ID);
        }
        $somevariable = get_field('features_key_points',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['features_key_points']= implode(', ', get_field('features_key_points',$post->ID));
        }
        $somevariable = get_field('read_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['read_site_url']= get_field('read_site_url',$post->ID);
        }
        //view fields
        $somevariable = get_field('description_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['description_view']= get_field('description_view',$post->ID);
        }
        $somevariable = get_field('features_seen_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['features_seen_view'] = implode(', ', get_field('features_seen_view',$post->ID));
        }
        $somevariable = get_field('view_image_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['view_image_url']= get_field('view_image_url',$post->ID);
        }
        $somevariable = get_field('view_type',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['view_type']= get_field('view_type',$post->ID);
        }
        //play fields
        $somevariable = get_field('description_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['description_of_game']= get_field('description_of_game',$post->ID);
        }
        $somevariable = get_field('type_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['type_of_game']= implode(', ', get_field('type_of_game',$post->ID));
        }
        $somevariable = get_field('estimated_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['estimated_time']= get_field('estimated_time',$post->ID);
        }
        $somevariable = get_field('skill_level',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['skill_level']= get_field('skill_level',$post->ID);
        }
        /*Gamify fields only for play*/
        $somevariable = get_field('base_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['base_url']= get_field('base_url',$post->ID);
        }
        $somevariable = get_field('partner_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['partner_id']= get_field('partner_id',$post->ID);
        }
        $somevariable = get_field('partner_token',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['partner_token']= get_field('partner_token',$post->ID);
        }
        $somevariable = get_field('user_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['user_id']= get_field('user_id',$post->ID);
        }
        $somevariable = get_field('city_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['city_id']= get_field('city_id',$post->ID);
        }
        $somevariable = get_field('treasure_hunt_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['treasure_hunt_id']= get_field('treasure_hunt_id',$post->ID);
        }
        $somevariable = get_field('game_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['game_site_url']= get_field('game_site_url',$post->ID);
        }
        $somevariable = implode('', get_field('promote',$post->ID));
        if(!empty($somevariable) OR $somevariable != ''){ 
          if(get_field('promote',$post->ID) !='')
          $finalPostSearch[$j]['promote']= true;
        } else {
          $finalPostSearch[$j]['promote']= false;
        }
        $somevariable = implode('', $post->custom_fields->post_ad_field);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['post_ad_field']= implode('', $post->custom_fields->post_ad_field);
        } else {
          $finalPostSearch[$j]['post_ad_field']= "";
        }
        $somevariable = get_field('organiser',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['organiser']= get_field('organiser',$post->ID);
        }
        $somevariable = get_field('organiser_image',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['organiser_image']= get_field('organiser_image',$post->ID);
        }
        $destinationName = get_the_category($POSTID);
        $finalPostSearch[$j]['destination_title']= $destinationName[0]->name;
        $finalPostSearch[$j]['destination_booking']= implode(get_term_meta( $destinationName[0]->term_id, 'booking'));
        $j++;
      }
      if (empty($finalPostSearch)) {
       return ['status' => '0', 'message' => 'No Results Found'];
      }
      else{
        return ['results' => $finalPostSearch, 'status' => '1' ];
      }
    }
    // print_r($searchResult);
    $searchQuery= [];
    foreach ($searchResult as $exist) {
      $searchQuery['value'] = $exist->term_id;
    }
    $searchString = implode(' ', $searchQuery);
    // print_r($searchString);
    $posts = get_posts(array(
      'cat' => $searchString,
      'post_type' => array('post'),
      'meta_key'      => 'promote',
      'orderby'     => 'meta_value',
      'order' => 'DESC',
      'posts_per_page' => '-1'
    ));
    $finalSearch = []; $j=0;
    foreach ($posts as $key => $post) {
      $POSTID = $post->ID;
      $finalSearch[$j]['id'] = $post->ID;
      $finalSearch[$j]['slug'] = $post->post_name;
      $finalSearch[$j]['title'] = $post->post_title;
      $finalSearch[$j]['type'] = $post->post_type;
      $finalSearch[$j]['url'] = get_permalink($POSTID);
      $finalSearch[$j]['status'] = $post->post_status;
      $finalSearch[$j]['content'] = $post->post_content;
      $finalSearch[$j]['tags'] = $post->tags;
      $finalSearch[$j]['comments'] = $post->comments;
      $finalSearch[$j]['comment_status'] = $post->comment_status;
      //condition for thumbnail
      $postImage = get_the_post_thumbnail_url($post->ID);
      if(empty($postImage) OR $postImage == ''){
        $finalSearch[$j]['featured_image'] = $this->defaultLogo;
      }
      else{
        $finalSearch[$j]['featured_image'] = get_the_post_thumbnail_url($post->ID, 'full_image');
      }
      $postThumbnail = get_post_thumbnail_id($post->ID);
      if(empty($postThumbnail) OR $postThumbnail == ''){
        $finalSearch[$j]['thumbnail'] = $this->defaultLogo;
      }
      else{
        $finalSearch[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
      }
      //run fields
      $somevariable = get_field('description',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['description']= get_field('description',$post->ID);
      }
      $somevariable = get_field('distance',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['distance']= get_field('distance',$post->ID);
      }
      $somevariable = get_field('route',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['route']= implode(', ', get_field('route',$post->ID));
      }
      $somevariable = get_field('video_play_back_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['video_play_back_time']= get_field('video_play_back_time',$post->ID);
      }
      $somevariable = get_field('features_of_route',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['features_of_route'] = implode(', ', get_field('features_of_route',$post->ID));
      }
      $somevariable = get_field('gpx_file',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['gpx_file']= get_field('gpx_file',$post->ID);
      }
      //read fields
      $somevariable = get_field('description_read',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['description_read']= get_field('description_read',$post->ID);
      }
      $somevariable = get_field('estimated_reading_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['estimated_reading_time']= get_field('estimated_reading_time',$post->ID);
      }
      $somevariable = get_field('features_key_points',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['features_key_points']= implode(', ', get_field('features_key_points',$post->ID));
      }
      $somevariable = get_field('read_site_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['read_site_url']= get_field('read_site_url',$post->ID);
      }
      //view fields
      $somevariable = get_field('description_view',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['description_view']= get_field('description_view',$post->ID);
      }
      $somevariable = get_field('features_seen_view',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['features_seen_view'] = implode(', ', get_field('features_seen_view',$post->ID));
      }
      $somevariable = get_field('view_image_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['view_image_url']= get_field('view_image_url',$post->ID);
      }
      $somevariable = get_field('view_type',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['view_type']= get_field('view_type',$post->ID);
      }
      //play fields
      $somevariable = get_field('description_of_game',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['description_of_game']= get_field('description_of_game',$post->ID);
      }
      $somevariable = get_field('type_of_game',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['type_of_game']= implode(', ', get_field('type_of_game',$post->ID));
      }
      $somevariable = get_field('estimated_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['estimated_time']= get_field('estimated_time',$post->ID);
      }
      $somevariable = get_field('skill_level',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['skill_level']= get_field('skill_level',$post->ID);
      }
      /*Gamify fields only for play*/
      $somevariable = get_field('base_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['base_url']= get_field('base_url',$post->ID);
      }
      $somevariable = get_field('partner_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['partner_id']= get_field('partner_id',$post->ID);
      }
      $somevariable = get_field('partner_token',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['partner_token']= get_field('partner_token',$post->ID);
      }
      $somevariable = get_field('user_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['user_id']= get_field('user_id',$post->ID);
      }
      $somevariable = get_field('city_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['city_id']= get_field('city_id',$post->ID);
      }
      $somevariable = get_field('treasure_hunt_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['treasure_hunt_id']= get_field('treasure_hunt_id',$post->ID);
      }
      $somevariable = get_field('game_site_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['game_site_url']= get_field('game_site_url',$post->ID);
      }
      $somevariable = implode('', get_field('promote',$post->ID));
      if(!empty($somevariable) OR $somevariable != ''){ 
        if(get_field('promote',$post->ID) !='')
        $finalSearch[$j]['promote']= true;
      } else {
          $finalSearch[$j]['promote']= false;
      }
      $somevariable = implode('', $post->custom_fields->post_ad_field);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['post_ad_field']= implode('', $post->custom_fields->post_ad_field);
      } else{
        $finalSearch[$j]['post_ad_field']= "";
      }
      $somevariable = get_field('organiser',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['organiser']= get_field('organiser',$post->ID);
      }
      $somevariable = get_field('organiser_image',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['organiser_image']= get_field('organiser_image',$post->ID);
      }
      $destinationName = get_the_category($POSTID);
        $finalSearch[$j]['destination_title']= $destinationName[0]->name;
        $finalPostSearch[$j]['destination_booking']= implode(get_term_meta( $destinationName[0]->term_id, 'booking'));
      $j++;
    }
    if (empty($finalSearch)) {
     return ['status' => '0', 'message' => 'No Results Found'];
    }
    else{
      return ['results' => $finalSearch, 'status' => '1' ]; 
    }
  }

  function feedback(){
    global $wpdb;
    $userName = $_REQUEST['user_name'];
    $email = $_REQUEST['user_email'];
    $feedback = $_REQUEST['feedback'];
    $mes = array(
      'Name: ' => 'Name: '.$userName,
      'Email: ' => 'Email: '.$email,
      'Feedback: ' => 'Feedback: '.$feedback
    );
    $message = implode("\n", $mes);
    $to = "meenaakshi@appliedsyntax.io";
    $subject = "Runcation Feedback";
    $headers = '';
    $query = "INSERT INTO feedBack (`userEmail`,`feedback`) VALUES ('$email','$message')";
      $result = $wpdb->query($query);

    $sent_message = wp_mail($to, $subject, $message, $headers);
    if($sent_message){
      return ['message' => 'Mail sent successfully', 'status' => "Ok"];
    }
    else{
      return ['message' => 'Mail unsucessfull', 'status' => 0 ];
    }
  }
  
  function add_favorites(){
    global $wpdb;
    $email = $_REQUEST['user_email'];
    $postId = $_REQUEST['post_id'];
    $postType = $_REQUEST['favorite_status'];
    
      $query = "INSERT INTO usersFavorites (userEmail,postId,favorite_status) VALUES ('$email','$postId','$postType')";
      $result = $wpdb->query($query);
      if ($result == 1){
        $data["message"] = "data saved successfully";
        $data["status"] = "Ok";
      }
      else{
        $data["message"] = "enter user details";
        $data["status"] = "error";    
      }
    return $data;
  }

  function get_favorites(){
    global $wpdb;
    $email = $_REQUEST['user_email'];

    $categoryQuery = "SELECT postId FROM usersFavorites WHERE userEmail='$email' and favorite_status='0' ORDER BY wpUsersId DESC";
    $resultCategory = $wpdb->get_results($categoryQuery);
    $categoryFavorites= [];
    if ($resultCategory) {
      $categoryDetails = []; 
      foreach ( $resultCategory as $catID ) { 
          $categoryDetails[] = get_category( $catID->postId );    
      }

      $images = get_option('taxonomy_image_plugin');
      for($i = 2 ; $i <=count($images)+1; $i++){
       $img_url[$i]= wp_get_attachment_url( $images[$i] );
      }
      //check array if null
      foreach ($categoryDetails as $som => $catNames) {
        if (empty($catNames)) {
          unset($categoryDetails[$som]);
        }
      }
      $rd=0;
      foreach ($categoryDetails as $catNames) {
        $categoryFavorites[$rd]['id'] = $catNames->term_id;
        $categoryFavorites[$rd]['name'] = $catNames->name;
        $categoryFavorites[$rd]['location'] = implode( get_term_meta( $catNames->term_id, 'location'));
          $categoryThumbnail = wp_get_attachment_url( $images[$catNames->term_id] );
          if (empty($categoryThumbnail) OR $categoryThumbnail == '') {
            $categoryFavorites[$rd]['image'] = $this->defaultLogo;
          }
          else{
            $categoryFavorites[$rd]['image'] = wp_get_attachment_url( $images[$catNames->term_id] );
          }
        $categoryFavorites[$rd]['description'] = $catNames->description;
        $categoryFavorites[$rd]['booking'] = implode( get_term_meta( $catNames->term_id, 'booking'));
        $categoryFavorites[$rd]['fav_type'] = 'Destination';
        $categoryFavorites[$rd]['promote'] = false;
        $rd++;
      }
    }
    //post favorites starts here
    $postQuery = "SELECT postId FROM usersFavorites WHERE userEmail='$email' and favorite_status='1' ORDER BY wpUsersId DESC";
    $resultPost = $wpdb->get_results($postQuery);
    // if (empty($resultPost)) {
    //   return ['message' => 'Please enter user details', 'status' => '0'];
    // }
    $postFavorites = [];
    if($resultPost){
      $postIds=[]; $i=0;
      foreach ($resultPost as $pIDS) {
        $postIds[$i] =$pIDS->postId;
        $i++;
      } 
      global $json_api, $post;
      $args = array( 
       'post_type' => array('run','read','play','view'), // must
       'post__in' => $postIds,
       'orderby' => 'post__in',
        'order'=> 'DESC',
        'posts_per_page' => '-1'
      );
      $posts = get_posts($args);
        // print_r($posts);
       $j=0;
      foreach ($posts as $post) {
        $destinationName = get_the_category($post->ID);
        $postFavorites[$j]['id'] = $post->ID;
        $postFavorites[$j]['title'] = $post->post_title;
        $postFavorites[$j]['type'] = $post->post_type;
        $postFavorites[$j]['slug'] = $post->post_name;
        $postFavorites[$j]['url'] = get_permalink($post->ID);
        $postFavorites[$j]['status'] = $post->post_status;
        $postFavorites[$j]['content'] = $post->post_content;
        $postFavorites[$j]['tags'] = $post->tags;
        $postFavorites[$j]['date'] = $post->post_date;
        $postFavorites[$j]['modified'] = $post->post_modified;
        $postFavorites[$j]['comments'] = $post->comments;
        $postFavorites[$j]['comment_status'] = $post->comment_status;         
        // $postFavorites[$j]['featured_image'] = 'https://runcation.in/admin/wp-content/uploads/2018/12/default-featured-image.png';
          $postImage = get_the_post_thumbnail_url($post->ID);
        if(empty($postImage) OR $postImage == ''){
          $postFavorites[$j]['featured_image'] = $this->defaultLogo;
        }
        else{
          $postFavorites[$j]['featured_image'] = get_the_post_thumbnail_url($post->ID, 'full_image');
        }
        $postThumbnail = get_the_post_thumbnail_url($post->ID);
        if(empty($postThumbnail) OR $postThumbnail == ''){
          $postFavorites[$j]['thumbnail'] = $this->defaultLogo;
        }
        else{
          $postFavorites[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
        }
        //run fields
        $somevariable = get_field('description',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['description']= get_field('description',$post->ID);
        }
        $somevariable = get_field('distance',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['distance']= get_field('distance',$post->ID);
        }
        $somevariable = get_field('route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['route']= implode(', ', get_field('route',$post->ID));
        }
        $somevariable = get_field('video_play_back_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['video_play_back_time']= get_field('video_play_back_time',$post->ID);
        }
        $somevariable = get_field('features_of_route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['features_of_route'] = implode(', ', get_field('features_of_route',$post->ID));
        }
        $somevariable = get_field('gpx_file',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['gpx_file']= get_field('gpx_file',$post->ID);
        }
        //read fields
        $somevariable = get_field('description_read',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['description_read']= get_field('description_read',$post->ID);
        }
        $somevariable = get_field('estimated_reading_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['estimated_reading_time']= get_field('estimated_reading_time',$post->ID);
        }
        $somevariable = get_field('features_key_points',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['features_key_points']= implode(', ', get_field('features_key_points',$post->ID));
        }
        $somevariable = get_field('read_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['read_site_url']= get_field('read_site_url',$post->ID);
        }
        //view fields
        $somevariable = get_field('description_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['description_view']= get_field('description_view',$post->ID);
        }
        $somevariable = get_field('features_seen_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['features_seen_view'] = implode(', ', get_field('features_seen_view',$post->ID));
        }
        $somevariable = get_field('view_image_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['view_image_url']= get_field('view_image_url',$post->ID);
        }
        $somevariable = get_field('view_type',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['view_type']= get_field('view_type',$post->ID);
        }
        //the play fields
        $somevariable = get_field('description_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['description_of_game']= get_field('description_of_game',$post->ID);
        }
        $somevariable = get_field('type_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['type_of_game']= implode(', ', get_field('type_of_game',$post->ID));
        }
        $somevariable = get_field('estimated_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['estimated_time']= get_field('estimated_time',$post->ID);
        }
        $somevariable = get_field('skill_level',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['skill_level']= get_field('skill_level',$post->ID);
        }
        /*Gamify fields only for play*/
        $somevariable = get_field('base_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['base_url']= get_field('base_url',$post->ID);
        }
        $somevariable = get_field('partner_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['partner_id']= get_field('partner_id',$post->ID);
        }
        $somevariable = get_field('partner_token',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['partner_token']= get_field('partner_token',$post->ID);
        }
        $somevariable = get_field('user_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['user_id']= get_field('user_id',$post->ID);
        }
        $somevariable = get_field('city_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['city_id']= get_field('city_id',$post->ID);
        }
        $somevariable = get_field('treasure_hunt_id',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['treasure_hunt_id']= get_field('treasure_hunt_id',$post->ID);
        }
        $somevariable = get_field('game_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['game_site_url']= get_field('game_site_url',$post->ID);
        }
        $somevariable = implode('', get_field('promote',$post->ID));
        // if(!empty($somevariable) OR $somevariable != ''){ 
          if(get_field('promote',$post->ID) !='') {
            $postFavorites[$j]['promote']= true;
          } else {
            $postFavorites[$j]['promote']= false;
          }
        // }
        $somevariable = get_field('organiser',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['organiser']= get_field('organiser',$post->ID);
        }
        $somevariable = get_field('organiser_image',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $postFavorites[$j]['organiser_image']= get_field('organiser_image',$post->ID);
        }
        $catID = []; $k=0;
        foreach ($destinationName as $name) {
          $catID1 = $name->name;
          $catID2= implode( get_term_meta( $name->term_id, 'location'));
          $k++;
        }
        $postFavorites[$j]['cat_title'] = $catID1; 
        $postFavorites[$j]['location'] = $catID2;
        $postFavorites[$j]['fav_type'] = "Content";
        $j++;
      }
    }

    $categoryFavorites = array_unique($categoryFavorites, SORT_REGULAR);
    $favoritesArray =  array_merge($categoryFavorites, $postFavorites);
    if (is_array($favoritesArray) && !empty($favoritesArray) ) {
      return ['results' => $favoritesArray, 'status' => '1'];
    }
    else{
      return ['message' => "No favorites found", 'status' => '0'];
    }
  }

  function get_count_favorites(){
    global $wpdb;
    
    $email = $_REQUEST['user_email'];
    $countQueries = $wpdb->get_results("SELECT postId FROM `usersFavorites` WHERE `userEmail`= '$email'");
    $fav = 0;
    foreach ($countQueries as $countQuery) {
      $pid = $countQuery->postId;
      $post_status = get_post_status($pid);

      $fav_status =  "SELECT favorite_status FROM `usersFavorites` WHERE `userEmail`= '$email' and postId='$pid'";
      $favQuery = $wpdb->query($fav_status);

      if ($post_status == 'trash') {
        $query = "DELETE FROM usersFavorites WHERE userEmail='$email' and postId='$pid'";
        $deleteQuery = $wpdb->query($query);
      }
      elseif($post_status == false) {
        $query = "DELETE FROM usersFavorites WHERE userEmail='$email' and postId='$pid' and favorite_status='$favQuery' ";
        $deleteQuery = $wpdb->query($query);
      }
      $fav++;
    }

    $countFavorites = $wpdb->get_results("SELECT count(*) as favoritesCount FROM `usersFavorites` WHERE `userEmail`= '$email'");
    $count = [];
    foreach ($countFavorites as $favCounts) {
      $count['favoritesCount'] = $favCounts->favoritesCount;
    }
    $countString = implode(' ', $count);

    if ($countString >= 1) {
      return [ 'count_favorites' => $countString, 'status' => "1"];
    }
    else{
      return [ 'count_favorites' => "No favorites found", 'status' => "0"];
    }
  }

  function get_favorites_exsist(){
    global $wpdb;
    $email = $_REQUEST['user_email'];
    $postID = $_REQUEST['post_id'];

    $exsistQuery = "SELECT count(*) as value FROM usersFavorites WHERE userEmail = '$email' and postId = '$postID'";
    $exsistQuery = $wpdb->get_results($exsistQuery);

    $favQuery= [];
    foreach ($exsistQuery as $exist) {
      $favQuery['value'] = $exist->value;
    }
    $favoriteString = implode(' ', $favQuery);

    if ($favoriteString >= 1) {
      $data["status"] = "1";
      $data["message"] = "Favorites all ready exist";
    }
    else{
      $data["message"] = "Favorites do not exist";
      $data["status"] = "0";
    }
    return $data;
  }

  function delete_favorites(){
    global $wpdb;
    $userEmail = $_REQUEST['user_email'];
    $postID = $_REQUEST['post_id'];

    $query = "DELETE FROM usersFavorites WHERE userEmail='$userEmail' and postId='$postID'";
    $deleteQuery = $wpdb->query($query);

    if ($deleteQuery >= 1) {
      return ['delete_favorites'=> $deleteQuery, 'status' =>"1"];
    }
    else{
      return ['delete_favorites' => "No favorites record found", 'status' => "0"];
    }
  }

  function get_notifications(){
    global $wpdb;
    $query = array(
      'date_query'     => array(
        //only last previous week posts
        'after' => '1 week ago',
      ),
      'order' => 'DESC',
      'post_type' => array('run', 'play', 'read', 'view'),
      'posts_per_page'  => 20,
      'post_status' => 'publish',
      
    );

    $posts = get_posts($query);

      $notify = []; $i = 0;
    foreach ($posts as $key => $post) {
      $notify[$i]['post_id'] = $post->ID;
      $notify[$i]['post_title'] = $post->post_title;
      $notify[$i]['post_type'] = $post->post_type;
        $dates[] = $post->post_date;
        foreach ($dates as $date) {
          $datestr = $post->post_date;
          $dates1 = date_create("$datestr");
          $finalDate = date_format($dates1,"d M Y, h:i a");
        }
      $notify[$i]['post_date'] = $finalDate;
      $postThumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
      if(empty($postThumbnail) OR $postThumbnail == ''){
        $notify[$i]['thumbnail'] = $this->defaultLogo;
      }
      else{
        $notify[$i]['thumbnail'] = wp_get_attachment_url( get_post_thumbnail_id($post->ID));  
      }
      //run fields
      $somevariable = get_field('description',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $notify[$i]['description']= get_field('description',$post->ID);
      }
      $somevariable = get_field('description_read',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $notify[$i]['description_read']= get_field('description_read',$post->ID);
      }
      $somevariable = get_field('description_view',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $notify[$i]['description_view']= get_field('description_view',$post->ID);
      }
      $somevariable = get_field('description_of_game',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $notify[$i]['description_of_game']= get_field('description_of_game',$post->ID);
      }
      $i++;
    }
    if (empty($notify)) {
     return ['status' => 0, 'message' => 'No Notifications'];
    }
    else{
      return ['results' => $notify, 'status' => 1 ]; 
    }
  }

  function get_notifications_new(){
    global $wpdb;
    $query = array(
      'date_query'     => array(
        //only last previous week posts
        'after' => '1 week ago',
      ),
      'order' => 'DESC',
      'post_type' => array('run', 'play', 'read', 'view'),
      'posts_per_page'  => 20,
      'post_status' => 'publish',
      
    );

    $posts = get_posts($query);
    // print_r($posts);
    $notify = []; $i = 0;
    foreach ($posts as $key => $post) {
      $notify[$i]['id'] = $post->ID;
      $notify[$i]['title'] = $post->post_title;
      $notify[$i]['type'] = $post->post_type;
      $notify[$i]['slug'] = $post->post_name;
      $notify[$i]['status'] = $post->post_status;
      $notify[$i]['content'] = $post->post_content;
      $notify[$i]['comments'] = $post->comments;
      $notify[$i]['comment_status'] = $post->comment_status;
        $dates[] = $post->post_date;
        foreach ($dates as $date) {
          $datestr = $post->post_date;
          $dates1 = date_create("$datestr");
          $finalDate = date_format($dates1,"d M Y, h:i a");
        }
      $notify[$i]['date'] = $finalDate;
      
      $postThumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
      if(empty($postThumbnail) OR $postThumbnail == ''){
        $notify[$i]['thumbnail'] = $this->defaultLogo;
      }
      else{
        $notify[$i]['thumbnail'] = wp_get_attachment_url( get_post_thumbnail_id($post->ID));  
      }
        //run fields
        $somevariable = get_field('description',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['description']= get_field('description',$post->ID);
        }
        $somevariable = get_field('distance',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['distance']= get_field('distance',$post->ID);
        }
        $somevariable = get_field('route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['route']= implode(', ', get_field('route',$post->ID));
        }
        $somevariable = get_field('video_play_back_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['video_play_back_time']= get_field('video_play_back_time',$post->ID);
        }
        $somevariable = get_field('features_of_route',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['features_of_route'] = implode(', ', get_field('features_of_route',$post->ID));
        }
        $somevariable = get_field('gpx_file',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['gpx_file']= get_field('gpx_file',$post->ID);
        }
        //read fields
        $somevariable = get_field('description_read',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['description_read']= get_field('description_read',$post->ID);
        }
        $somevariable = get_field('estimated_reading_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['estimated_reading_time']= get_field('estimated_reading_time',$post->ID);
        }
        $somevariable = get_field('features_key_points',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['features_key_points']= implode(', ', get_field('features_key_points',$post->ID));
        }
        $somevariable = get_field('read_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['read_site_url']= get_field('read_site_url',$post->ID);
        }
        //view fields
        $somevariable = get_field('description_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['description_view']= get_field('description_view',$post->ID);
        }
        $somevariable = get_field('features_seen_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['features_seen_view'] = implode(', ', get_field('features_seen_view',$post->ID));
        }
        $somevariable = get_field('view_image_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['view_image_url']= get_field('view_image_url',$post->ID);
        }
        $somevariable = get_field('view_type',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['view_type']= get_field('view_type',$post->ID);
        }
        $somevariable = get_field('route_view',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['route_view']= implode(', ', get_field('route_view',$post->ID));
        }
        //the play fields
        $somevariable = get_field('description_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['description_of_game']= get_field('description_of_game',$post->ID);
        }
        $somevariable = get_field('type_of_game',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['type_of_game']= implode(', ', get_field('type_of_game',$post->ID));
        }
        $somevariable = get_field('estimated_time',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['estimated_time']= get_field('estimated_time',$post->ID);
        }
        $somevariable = get_field('skill_level',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['skill_level']= get_field('skill_level',$post->ID);
        }
        /*Gamify fields only for play*/
        $somevariable = implode('', $post->custom_fields->base_url);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['base_url']= implode('', $post->custom_fields->base_url);
        }
        $somevariable = implode('', $post->custom_fields->partner_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['partner_id']= implode('', $post->custom_fields->partner_id);
        }
        $somevariable = implode('', $post->custom_fields->partner_token);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['partner_token']= implode('', $post->custom_fields->partner_token);
        }
        $somevariable = implode('', $post->custom_fields->user_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['user_id']= implode('', $post->custom_fields->user_id);
        }
        $somevariable = implode('', $post->custom_fields->city_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['city_id']= implode('', $post->custom_fields->city_id);
        }
        $somevariable = implode('', $post->custom_fields->treasure_hunt_id);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $newpostarray[$i]['treasure_hunt_id']= implode('', $post->custom_fields->treasure_hunt_id);
        }
        $somevariable = get_field('game_site_url',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['game_site_url']= get_field('game_site_url',$post->ID);
        }
        $somevariable = implode('', get_field('promote',$post->ID));
        if(!empty($somevariable) OR $somevariable != ''){ 
          if(get_field('promote',$post->ID) !='')
          $notify[$i]['promote']= true;
        } else {
          $notify[$i]['promote']= false;
        }
        $somevariable = get_field('organiser',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['organiser']= get_field('organiser',$post->ID);
        }
        $somevariable = get_field('organiser_image',$post->ID);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $notify[$i]['organiser_image']= get_field('organiser_image',$post->ID);
        }
      $i++;
    }
    if (empty($notify)) {
     return ['status' => 0, 'message' => 'No Notifications'];
    }
    else{
      return ['results' => $notify, 'status' => 1 ]; 
    }
  }

  public function add_completed_runs(){
    global $wpdb;
    $postID = $_REQUEST['post_id'];
    $email = $_REQUEST['user_email'];
    if (empty($postID) && empty($email)) {
      $errorlog["message"] = "enter user details";
      $errorlog["status"] = "error";   
    } 
    elseif (empty($postID)) {
      $errorlog["message"] = "enter postID";
      $errorlog["status"] = "error";
    } 
    elseif (empty($email)) {
      $errorlog["message"] = "enter user email";
      $errorlog["status"] = "error";
    } 
    else{
      $exsistQuery = "SELECT count(*) as value FROM completedRuns WHERE userEmail = '$email' and postId = '$postID'";
      $exsistQuery = $wpdb->get_results($exsistQuery);

      $completeQuery= [];
      foreach ($exsistQuery as $exist) {
        $completeQuery['value'] = $exist->value;
      }
      $completeString = implode(' ', $completeQuery);

      if ($completeString == 1) {
        return ['status' => 0, 'message' => 'Completed Runs exits'];
      }

      date_default_timezone_set('Asia/Kolkata');
      $timestamp = time();
      $dateTime = date("Y-m-d H:i:s", $timestamp);

      $query = "INSERT INTO completedRuns (`userEmail`, `postID`, `dateTime`) VALUES ('$email', '$postID', '$dateTime')";
      $result = $wpdb->query($query);
        if (!empty($result)){
          $data["message"] = "data saved successfully";
          $data["status"] = "Ok";
        }
        else{
          $data["message"] = "enter user details";
          $data["status"] = "error";    
        }
      return $data;
    }
    return $errorlog;
  }

  public function get_completed_runs(){
    global $wpdb;
    $email = $_REQUEST['user_email'];
    $sqlquery = "SELECT postID FROM completedRuns WHERE userEmail='$email' ORDER BY dateTime DESC";
    
    $result = $wpdb->get_results($sqlquery);
    // print_r($result);
    if (empty($result)) {
      return ['status' => 0, 'message' => 'No Completed Runs Found'];
    }
    
    $res=[]; $i=0;
    foreach ($result as $key => $value) {
      $res[$i] =$value->postID;
      $i++;
    }
    // print_r($res);
    global $json_api, $post;
    $args = array( 
     'post_type' => array('run'), // must
     'post__in' => $res,
     'orderby' => 'post__in',
     'order'=> 'DESC',
     'posts_per_page' => 50
    );
    // print_r($args);
    $posts = get_posts($args);
    // print_r($posts);
    $completedRun = []; $j=0;
    foreach ($posts as $key => $post) {
      $POSTID = $post->ID;
      $destinationName = get_the_category($post->ID);
      $completedRun[$j]['id'] = $post->ID;
      $completedRun[$j]['slug'] = $post->post_name;
      $completedRun[$j]['title'] = $post->post_title;
      $completedRun[$j]['type'] = $post->post_type;
      $completedRun[$j]['url'] = get_permalink($POSTID);
      $completedRun[$j]['status'] = $post->post_status;
      $completedRun[$j]['content'] = $post->post_content;
      $completedRun[$j]['tags'] = $post->tags;
      $completedRun[$j]['comments'] = $post->comments;
      $completedRun[$j]['comment_status'] = $post->comment_status;
      //condition for thumbnail
      $postImage = get_the_post_thumbnail_url($post->ID);
      if(empty($postImage) OR $postImage == ''){
        $completedRun[$j]['featured_image'] = $this->defaultLogo;
      }
      else{
        $completedRun[$j]['featured_image'] = get_the_post_thumbnail_url($post->ID, 'full_image');
      }
      $postThumbnail = get_post_thumbnail_id($post->ID);
      if(empty($postThumbnail) OR $postThumbnail == ''){
        $completedRun[$j]['thumbnail'] = $this->defaultLogo;
      }
      else{
        $completedRun[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
      }
      //run fields
      $somevariable = get_field('description',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['description']= get_field('description',$post->ID);
      }
      $somevariable = get_field('distance',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['distance']= get_field('distance',$post->ID);
      }
      $somevariable = get_field('route',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['route']= implode(', ', get_field('route',$post->ID));
      }
      $somevariable = get_field('video_play_back_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['video_play_back_time']= get_field('video_play_back_time',$post->ID);
      }
      $somevariable = get_field('features_of_route',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['features_of_route'] = implode(', ', get_field('features_of_route',$post->ID));
      }
      $somevariable = get_field('gpx_file',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['gpx_file']= get_field('gpx_file',$post->ID);
      }
      //read fields
      $somevariable = get_field('description_read',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['description_read']= get_field('description_read',$post->ID);
      }
      $somevariable = get_field('estimated_reading_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['estimated_reading_time']= get_field('estimated_reading_time',$post->ID);
      }
      $somevariable = get_field('features_key_points',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['features_key_points']= implode(', ', get_field('features_key_points',$post->ID));
      }
      $somevariable = get_field('read_site_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['read_site_url']= get_field('read_site_url',$post->ID);
      }
      //view fields
      $somevariable = get_field('description_view',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['description_view']= get_field('description_view',$post->ID);
      }
      $somevariable = get_field('features_seen_view',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['features_seen_view'] = implode(', ', get_field('features_seen_view',$post->ID));
      }
      $somevariable = get_field('view_image_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['view_image_url']= get_field('view_image_url',$post->ID);
      }
      $somevariable = get_field('view_type',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['view_type']= get_field('view_type',$post->ID);
      }
      //the play fields
      $somevariable = get_field('description_of_game',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['description_of_game']= get_field('description_of_game',$post->ID);
      }
      $somevariable = get_field('type_of_game',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['type_of_game']= implode(', ', get_field('type_of_game',$post->ID));
      }
      $somevariable = get_field('estimated_time',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['estimated_time']= get_field('estimated_time',$post->ID);
      }
      $somevariable = get_field('skill_level',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['skill_level']= get_field('skill_level',$post->ID);
      }
      /*Gamify fields only for play*/
      $somevariable = get_field('base_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['base_url']= get_field('base_url',$post->ID);
      }
      $somevariable = get_field('partner_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['partner_id']= get_field('partner_id',$post->ID);
      }
      $somevariable = get_field('partner_token',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['partner_token']= get_field('partner_token',$post->ID);
      }
      $somevariable = get_field('user_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['user_id']= get_field('user_id',$post->ID);
      }
      $somevariable = get_field('city_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['city_id']= get_field('city_id',$post->ID);
      }
      $somevariable = get_field('treasure_hunt_id',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['treasure_hunt_id']= get_field('treasure_hunt_id',$post->ID);
      }
      $somevariable = get_field('game_site_url',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['game_site_url']= get_field('game_site_url',$post->ID);
      }
      $somevariable = implode('', get_field('promote',$post->ID));
      if(!empty($somevariable) OR $somevariable != ''){ 
        if(get_field('promote',$post->ID) !='')
        $completedRun[$j]['promote']= true;
      } else {
        $completedRun[$j]['promote']= false;
      }
      $somevariable = get_field('organiser',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['organiser']= get_field('organiser',$post->ID);
      }
      $somevariable = get_field('organiser_image',$post->ID);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $completedRun[$j]['organiser_image']= get_field('organiser_image',$post->ID);
      }
      $date_time = "SELECT `dateTime` as complete_time FROM completedRuns WHERE `userEmail`='$email' and `postID` = $POSTID";
      $dateTime = $wpdb->get_results($date_time) or die(mysql_error());
      $catID = []; $k=0;
      foreach ($destinationName as $key => $name) {
        $catID1 = $name->name;
        $catID2= implode( get_term_meta( $name->term_id, 'location'));
        $k++;
      }
      $dates=[]; $l=0;
      foreach ($dateTime as $dateTimes) {
        $dates1 = $dateTimes->complete_time;
        $date=date_create($dates1);
        $finalDate = date_format($date,"d M Y, h:i a");
        $l++;
      }
      $completedRun[$j]['cat_title'] = $catID1; 
      $completedRun[$j]['location'] = $catID2;
      $completedRun[$j]['date_time'] = $finalDate;
      $j++;
    }
    if (empty($completedRun)) {
     return ['status' => 0, 'message' => 'No Completed Runs'];
    }
    else{
      return ['results' => $completedRun, 'status' => 1 ]; 
    }
  }

  function post_push_notifications() {
    global $wpdb;

    $allPosts = new WP_Query(
      array(
        'posts_per_page' =>1,
        'post_type' => array('run', 'read', 'play', 'view'),
        'post_status' => 'published',
        'meta_key' => 'send_notification',
        'order' => 'DESC',
        'orderby' => 'date',
      )
    );
    
    $notifyID = '';
    if ( $allPosts->have_posts() ) { 
        $shereo_count = $allPosts->post_count;
        while ( $allPosts->have_posts() ) {
            $allPosts->the_post(); 
            $notifyID = get_the_ID();
        }
    }
    // print_r($notifyID);
    $notification = get_field('send_notification',$notifyID);
    if (!empty($notification) && isset($notification)) {
      $notificationJsonArray = [];
        $notification_post = get_post($notifyID);
        // print_r($notification_post);
        $notificationJsonArray['notification_content']['name'] = 'PostID '.$notifyID;
        $notificationJsonArray['notification_content']['title'] = $notification_post->post_title;
        $somevariable = get_field('description',$notifyID);
        if (!empty($somevariable) OR $somevariable != '') {
          $notificationJsonArray['notification_content']['body'] = get_field('description',$notifyID);
        }
        $somevariable = get_field('description_read',$notifyID);
        if (!empty($somevariable) OR $somevariable != '') {
          $notificationJsonArray['notification_content']['body'] = get_field('description_read',$notifyID);
        }
        $somevariable = get_field('description_view',$notifyID);
        if (!empty($somevariable) OR $somevariable != '') {
          $notificationJsonArray['notification_content']['body'] = get_field('description_view',$notifyID);
        }
        $somevariable = get_field('description_of_game',$notifyID);
        if (!empty($somevariable) OR $somevariable != '') {
          $notificationJsonArray['notification_content']['body'] = get_field('description_of_game',$notifyID);
        }
        $notificationJsonArray['notification_content']['custom_data']['post_type'] = $notification_post->post_type;
        // print_r($notificationJsonArray);
        $json = json_encode($notificationJsonArray, JSON_UNESCAPED_SLASHES);
        // print_r($json);

        $url = 'https://appcenter.ms/api/v0.1/apps/ajay-gounesco.com/Runcation-Madhya-Pradesh-android/push/notifications';
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-API-Token: d636bd2a7aa3e50b11b4cc4e9b8b2ce15f322c70',
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, "POST");
        
        $result = curl_exec($ch);
        echo $result;
        curl_close($ch);
    }
  }

  function notiy_ios() {
    global $wpdb;
    /*get udid's*/
    $deviceNumberArray = [];
    $udidQuery = "SELECT deviceNumber FROM devices";
    $udidResult = $wpdb->get_results($udidQuery);
    $deviceNumberArray = $udidResult;
    // print_r($deviceNumberArray);
    $allPosts = new WP_Query(
      array(
        'posts_per_page' =>1,
        'post_type' => array('run', 'read', 'play', 'view'),
        'post_status' => 'published',
        'meta_key' => 'send_notification',
        'order' => 'DESC',
        'orderby' => 'date',
      )
    );
    
    $notifyID = '';
    if ( $allPosts->have_posts() ) { 
        $shereo_count = $allPosts->post_count;
        while ( $allPosts->have_posts() ) {
            $allPosts->the_post(); 
            $notifyID = get_the_ID();
        }
    }
    // print_r($notifyID);
    $notification = get_field('send_notification',$notifyID);
    if (!empty($notification) && isset($notification)) {

      $notificationJsonArray = [];
      $notification_post = get_post($notifyID);
      $notificationJsonArray['successful'] = '15';
      $notificationJsonArray['url'] = $notification_post->guid;
      $notificationJsonArray['data']['id'] = 'PostID '.$notifyID;
      $runTitle = $notificationJsonArray['data']['title'] = $notification_post->post_title;
      $somevariable = get_field('description',$notifyID);
      if (!empty($somevariable) OR $somevariable != '') {
        $desIos = $notificationJsonArray['data']['body'] = get_field('description',$notifyID);
      }
      $somevariable = get_field('description_read',$notifyID);
      if (!empty($somevariable) OR $somevariable != '') {
        $desIos = $notificationJsonArray['data']['body'] = get_field('description_read',$notifyID);
      }
      $somevariable = get_field('description_view',$notifyID);
      if (!empty($somevariable) OR $somevariable != '') {
        $desIos = $notificationJsonArray['data']['body'] = get_field('description_view',$notifyID);
      }
      $somevariable = get_field('description_of_game',$notifyID);
      if (!empty($somevariable) OR $somevariable != '') {
        $desIos = $notificationJsonArray['data']['body'] = get_field('description_of_game',$notifyID);
      }
      $notificationJsonArray['data']['post_type'] = $notification_post->post_type;
      $content = array(
        "en" => $runTitle
      );
      $headings = array(
        "en" => $desIos
      );
      $fields = array(
        'app_id' => "38da1114-e98e-445a-958e-b81ea3432eaf",
        'included_segments' => ["All"],
        'data' => $notificationJsonArray,
        'contents' => $headings,
        'subtitle' => $content
      );
      // print_r($notificationJsonArray);
      $json = json_encode($fields, JSON_UNESCAPED_SLASHES);
      // print_r($json);
      $url = 'https://onesignal.com/api/v1/notifications';
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json; charset=utf-8',
          'Authorization: Basic YjU0MzJlNjYtNzc4YS00OTQ2LWJlZjctZDI3MzQxMmNmN2Vm'
      ));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, "POST");
      
      $result = curl_exec($ch);
      echo $result;
      curl_close($ch);
    }
  }
}
