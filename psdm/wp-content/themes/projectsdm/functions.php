<?php
define('MY_THEME_FOLDER',str_replace("\\",'/',dirname(__FILE__)));
define('SITE_URL', get_site_url());//Site home url
define('THEME_PATH', get_template_directory_uri());//For images

require_once MY_THEME_FOLDER . '/config.php';