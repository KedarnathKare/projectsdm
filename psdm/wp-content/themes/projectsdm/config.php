<?php
function custom_theme_setup() {
add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );

function events_article_posttype(){
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'events' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array(
                      'title',
                      'editor',
                      'author',
                      'thumbnail',
                      'excerpt',
                      'comments',
                      'revisions',
                      'custom-fields',
                      'page-attributes'),
      'taxonomies'  => array( 'category' ),
    )
  );
}
add_action( 'init', 'events_article_posttype' );
/**
* Featured image
*/
if (function_exists('add_theme_support')) {
  add_theme_support('post-thumbnails', array( 'run', 'read', 'play', 'view' ) );  
  add_image_size( 'thumbnail_image', 256, 256, true);
  add_image_size( 'full_image', 1280, 640, true);
}
