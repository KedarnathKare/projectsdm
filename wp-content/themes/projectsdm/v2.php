<?php
/*
 Controller name: v2
 Controller description: JSON API v2 Controller
 */
class JSON_API_v2_Controller {

  public function get_category_details() {
    global $json_api;
    
    $images = get_option('taxonomy_image_plugin');
    for($i = 2 ; $i <=count($images)+1; $i++){
     $img_url[$i]= wp_get_attachment_url( $images[$i] );
    }
    $categories = $json_api->introspector->get_categories();

    $destinations =[]; $i = 0;
    foreach ($categories as $cat) {
      $destinations[$i]['id']= $cat->id;  
      $destinations[$i]['slug']= $cat->slug; 
      $destinations[$i]['title']= $cat->title; 
      $destinations[$i]['description']= $cat->description;
      $destinations[$i]['location']= implode( get_term_meta( $cat->id, 'location'));
      $destinations[$i]['booking']= implode(get_term_meta( $cat->id, 'booking'));
      $destinations[$i]['ad_link'] = implode(get_term_meta( $cat->id, 'ad_link'));
      $somevariable = $images[$cat->id];
      if(empty($somevariable) OR $somevariable == ''){
        $destinations[$i]['image']= $this->defaultLogo;
      }
      else{
        $destinations[$i]['image']= wp_get_attachment_url( $images[$cat->id] );
      }
      $i++;
    } 

    if(empty($destinations)) {
     return ['status' => 0, 'message' => 'No Events Available'];
    }
    else{
      return ['results' => $destinations, 'status' => 1  ];
    }
  }

  public function get_category_posts() {
    global $json_api;
    $category = $json_api->introspector->get_current_category();
    
    if (!$category) {
      $json_api->error("Not found.");
    }
    $posts = $json_api->introspector->get_posts(array(
      'cat' => $category->id,
      'post_type' => 'post',
      'meta_key'      => 'promote_event',
      'orderby'     => 'meta_value',
      'order' => 'DESC',
      'posts_per_page' => '-1'
    ));

    $newpostarray = []; $i=0;
    foreach ($posts as $key => $post) {
      $postID = $post->id;
      $newpostarray[$i]['id']= $postID;
      $newpostarray[$i]['title']= html_entity_decode(get_the_title($postID));
      $newpostarray[$i]['type']= get_post_type($postID);
      $newpostarray[$i]['slug']= $post->slug;
      $newpostarray[$i]['url']= get_post_permalink($postID);
      $newpostarray[$i]['status']= $post->status;
      $newpostarray[$i]['content']= $post->content;
      $newpostarray[$i]['date']= $post->date;
      $newpostarray[$i]['modified']= $post->modified;
      $newpostarray[$i]['tags']= $post->tags;
      $newpostarray[$i]['comments']= $post->comments;
      $newpostarray[$i]['comment_status']= $post->comment_status;
      
      $postImage = get_the_post_thumbnail_url($post->id);
      if(!empty($postImage) OR $postImage == ''){
        $newpostarray[$i]['thumbnail'] = get_the_post_thumbnail_url($post->id, 'thumbnail_image');
      }
      //this is run post
      $somevariable = implode('', $post->custom_fields->event_content);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_content']= implode('', $post->custom_fields->event_content);
      }
      $somevariable = implode('', $post->custom_fields->event_date);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_date']= implode('', $post->custom_fields->event_date);
      }
      $somevariable = implode('', $post->custom_fields->event_theme);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_theme']= implode(', ', get_field('event_theme',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_location);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_location']= implode('', $post->custom_fields->event_location);
      }
      $somevariable = implode('', $post->custom_fields->event_latitude);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_latitude']= implode(', ', get_field('event_latitude',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_longitude);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_longitude']= implode('', $post->custom_fields->event_longitude);
      }
      //this is read post
      $somevariable = implode('', $post->custom_fields->event_address);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_address']= implode('', $post->custom_fields->event_address);
      }
      $somevariable = implode('', $post->custom_fields->promote_event);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['promote_event']= implode('', $post->custom_fields->promote_event);
      }
      $somevariable = implode('', $post->custom_fields->emergency);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['emergency']= implode(', ', get_field('emergency',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_contact_number);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_contact_number']= implode('', $post->custom_fields->event_contact_number);
      }
      $somevariable = implode('', $post->custom_fields->event_email);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $newpostarray[$i]['event_email']= implode('', $post->custom_fields->event_email);
      }
    }
    if (empty($newpostarray)) {
     return ['status' => 0, 'message' => 'No Events Post Available'];
    }
    else{
      return ['results' => $newpostarray, 'status' => 1 ];
    }
  }

  public function get_search_results(){
    global $wpdb;
    $searchVariable = $_REQUEST['search'];
    if (empty($searchVariable)) {
      return ['status' => 0, 'message' => 'Enter atleast 3 letters'];
    }

    $searchSql = "SELECT term_id FROM wp_terms WHERE name LIKE '%$searchVariable%'";

    // print_r($searchSql);
    $searchResult = $wpdb->get_results($searchSql);

    if (empty($searchResult) || $searchResult == '') {
      $myposts = $wpdb->get_results( "SELECT ID FROM wp_posts WHERE post_title LIKE '%$searchVariable%'");
      if (empty($myposts) || $myposts == '') {
        return ['status' => 0, 'message' => 'Results not found'];
      }
      $searchPostQuery= []; $i=0;
      foreach ($myposts as $existPost) {
        $searchPostQuery[$i] = $existPost->ID;
        $i++;
      }
      // print_r($searchPostQuery);
      $postsSearch = get_posts(array(
        'post__in' => $searchPostQuery,
        'post_type' => array('post'),
        'meta_key'      => 'promote',
        'orderby'     => 'meta_value',
        'order' => 'DESC',
        'posts_per_page' => '-1'
      ));
      // print_r($postsSearch);
      $finalPostSearch = []; $j=0;
      foreach ($postsSearch as $key => $post) {
        $POSTID = $post->ID;
        $finalPostSearch[$j]['id'] = $post->ID;
        $finalPostSearch[$j]['slug'] = $post->post_name;
        $finalPostSearch[$j]['title'] = $post->post_title;
        $finalPostSearch[$j]['type'] = $post->post_type;
        $finalPostSearch[$j]['url'] = get_permalink($POSTID);
        $finalPostSearch[$j]['status'] = $post->post_status;
        $finalPostSearch[$j]['content'] = $post->post_content;
        $finalPostSearch[$j]['tags'] = $post->tags;
        $finalPostSearch[$j]['comments'] = $post->comments;
        $finalPostSearch[$j]['comment_status'] = $post->comment_status;
        //condition for thumbnail
        $postImage = get_the_post_thumbnail_url($post->ID);
        if(empty($postImage) OR $postImage == ''){
          $finalPostSearch[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
        }
        //run fields
        $somevariable = implode('', $post->custom_fields->event_content);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_content']= implode('', $post->custom_fields->event_content);
        }
        $somevariable = implode('', $post->custom_fields->event_date);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_date']= implode('', $post->custom_fields->event_date);
        }
        $somevariable = implode('', $post->custom_fields->event_theme);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_theme']= implode(', ', get_field('event_theme',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->event_location);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_location']= implode('', $post->custom_fields->event_location);
        }
        $somevariable = implode('', $post->custom_fields->event_latitude);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_latitude']= implode(', ', get_field('event_latitude',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->event_longitude);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_longitude']= implode('', $post->custom_fields->event_longitude);
        }
        //this is read post
        $somevariable = implode('', $post->custom_fields->event_address);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_address']= implode('', $post->custom_fields->event_address);
        }
        $somevariable = implode('', $post->custom_fields->promote_event);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['promote_event']= implode('', $post->custom_fields->promote_event);
        }
        $somevariable = implode('', $post->custom_fields->emergency);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['emergency']= implode(', ', get_field('emergency',$post->id));
        }
        $somevariable = implode('', $post->custom_fields->event_contact_number);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_contact_number']= implode('', $post->custom_fields->event_contact_number);
        }
        $somevariable = implode('', $post->custom_fields->event_email);
        if(!empty($somevariable) OR $somevariable != ''){ 
          $finalPostSearch[$j]['event_email']= implode('', $post->custom_fields->event_email);
        }
        $j++;
      }
      if (empty($finalPostSearch)) {
       return ['status' => '0', 'message' => 'No Results Found'];
      }
      else{
        return ['results' => $finalPostSearch, 'status' => '1' ];
      }
    }
    // print_r($searchResult);
    $searchQuery= [];
    foreach ($searchResult as $exist) {
      $searchQuery['value'] = $exist->term_id;
    }
    // print_r($searchQuery);
    $searchString = implode(' ', $searchQuery);
    
    $posts = get_posts(array(
      'cat' => $searchString,
      'post_type' => 'post',
      'meta_key'      => 'promote_event',
      'orderby'     => 'meta_value',
      'order' => 'DESC',
      'posts_per_page' => '-1'
    ));
    // print_r($posts);
    $finalSearch = []; $j=0;
    foreach ($posts as $key => $post) {
      $POSTID = $post->ID;
      $finalSearch[$j]['id'] = $post->ID;
      $finalSearch[$j]['slug'] = $post->post_name;
      $finalSearch[$j]['title'] = $post->post_title;
      $finalSearch[$j]['type'] = $post->post_type;
      $finalSearch[$j]['url'] = get_permalink($POSTID);
      $finalSearch[$j]['status'] = $post->post_status;
      $finalSearch[$j]['content'] = $post->post_content;
      $finalSearch[$j]['tags'] = $post->tags;
      $finalSearch[$j]['comments'] = $post->comments;
      $finalSearch[$j]['comment_status'] = $post->comment_status;
      //condition for thumbnail
      $postImage = get_the_post_thumbnail_url($post->ID);
      if(empty($postImage) OR $postImage == ''){
        $finalSearch[$j]['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail_image');
      }
      //run fields
      $somevariable = implode('', $post->custom_fields->event_content);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_content']= implode('', $post->custom_fields->event_content);
      }
      $somevariable = implode('', $post->custom_fields->event_date);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_date']= implode('', $post->custom_fields->event_date);
      }
      $somevariable = implode('', $post->custom_fields->event_theme);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_theme']= implode(', ', get_field('event_theme',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_location);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_location']= implode('', $post->custom_fields->event_location);
      }
      $somevariable = implode('', $post->custom_fields->event_latitude);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_latitude']= implode(', ', get_field('event_latitude',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_longitude);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_longitude']= implode('', $post->custom_fields->event_longitude);
      }
      //this is read post
      $somevariable = implode('', $post->custom_fields->event_address);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_address']= implode('', $post->custom_fields->event_address);
      }
      $somevariable = implode('', $post->custom_fields->promote_event);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['promote_event']= implode('', $post->custom_fields->promote_event);
      }
      $somevariable = implode('', $post->custom_fields->emergency);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['emergency']= implode(', ', get_field('emergency',$post->id));
      }
      $somevariable = implode('', $post->custom_fields->event_contact_number);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_contact_number']= implode('', $post->custom_fields->event_contact_number);
      }
      $somevariable = implode('', $post->custom_fields->event_email);
      if(!empty($somevariable) OR $somevariable != ''){ 
        $finalSearch[$j]['event_email']= implode('', $post->custom_fields->event_email);
      }
      $j++;
    }
    if (empty($finalSearch)) {
     return ['status' => '0', 'message' => 'No Results Found'];
    }
    else{
      return ['results' => $finalSearch, 'status' => '1' ]; 
    }
  }

}

?>